﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Tilemap : MonoBehaviour
{
    //Fields
    [SerializeField]
    private float tileWidthX = 1.0f;
    [SerializeField]
    private float tileWidthY = 1.0f;
    [SerializeField]
    private int tileCountX = 10;
    [SerializeField]
    private int tileCountY = 10;

    private Transform m_transform;
    public TileInfo[] tileInfoArray;
    public bool debugMode = false;


    //Methods
    void Start()
    {
        tileInfoArray = new TileInfo[tileCountX * tileCountY];
        
        m_transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (debugMode == true)
        {
            DrawDebugGrid();
        }
    }

    private void DrawDebugGrid()
    {
        if (m_transform == null)
        {
            m_transform.GetComponent<Transform>();
        }

        Vector3 horisontalLine = new Vector3(0, TileWidthY * TileCountY, 0);
        Vector3 verticalLine = new Vector3(TileWidthX * TileCountX, 0, 0);
        
        for (int i = 0; i <= TileCountX; i++)
        {
            Debug.DrawRay(new Vector3(m_transform.position.x + i*TileWidthX, m_transform.position.y, 0), horisontalLine, Color.green);
        }
        for (int i = 0; i <= TileCountY; i++)
        {
            Debug.DrawRay(new Vector3(m_transform.position.x, m_transform.position.y + i*TileWidthY, 0), verticalLine, Color.green);
        }

    }

    //Properties
    public float TileWidthX
    {
        get
        {
            return tileWidthX;
        }
    }

    public float TileWidthY
    {
        get
        {
            return tileWidthY;
        }
    }

    public int TileCountX
    {
        get
        {
            return tileCountX;
        }
    }

    public int TileCountY
    {
        get
        {
            return tileCountY;
        }
    }

}
