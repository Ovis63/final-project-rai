﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDEnums;
using System;

public class Turret : MonoBehaviour {

    private EnemyUnit m_target;
    [SerializeField]
    private SpawnManager m_world;

    [SerializeField]
    private GameObject m_cannonPart;            //The child of the turret that rotates and fires
    [SerializeField]
    private float m_adjustmentAngle = 270.0f;

    [Space(14)]
    [SerializeField]
    private bool m_continuousFire;
    [SerializeField]
    private float m_fireRate;           //Shots per minute. Not used if damage is continuous
    [SerializeField]
    private DamageType m_damageType;
    [SerializeField]
    private float m_damagePerShot;      //Damage per single shot. If fire is continuous, then it is damage per second instead
    [SerializeField]
    private float m_range;
    [SerializeField]
    private float m_trackSpeed;         //Degrees per second

    private float m_reloadingTime = 0.0f;

    void FixedUpdate()
    {
        //Search for the first target from the current enemy list that is within range
        List<EnemyUnit> tempWaveList = m_world.CurrentEnemyList;
        float maxDistTravelled = 0.0f;
        for (int i = 0; i < tempWaveList.Count; i++)
        {
            if (maxDistTravelled < tempWaveList[i].DistanceTravelled && Vector2.Distance(transform.position, tempWaveList[i].transform.position) <= m_range)
            {
                m_target = tempWaveList[i];
                maxDistTravelled = tempWaveList[i].DistanceTravelled;
            }
        }

        if (tempWaveList.Count != 0 && m_target == null)
        {
            m_target = tempWaveList[tempWaveList.Count - 1];
        }


        //Debug.DrawRay(transform.position, transform.up * m_range, Color.red);


        if (m_target != null)
        {

            //Rotate the turret towards target
            Vector2 diff = new Vector2(m_target.transform.position.x - transform.position.x, m_target.transform.position.y - transform.position.y);
            diff.Normalize();

            ////Working in radians
            //float targetRot = Mathf.PI * 2.0f - ((Mathf.PI * 3.5f + Mathf.Atan2(diff.y, diff.x)) % (Mathf.PI * 2.0f));
            //float currRot = Mathf.PI * 2 - (m_cannonPart.transform.rotation.eulerAngles.z * Mathf.Deg2Rad);
            //float deltaRot = targetRot - currRot;

            //if (deltaRot > Math.PI)
            //{
            //    deltaRot = Mathf.Abs(targetRot - currRot) - 2*Mathf.PI;
            //}
            //else if(deltaRot < -Math.PI)
            //{
            //    deltaRot = -Mathf.Abs(targetRot - currRot) + 2 * Mathf.PI;
            //}

            //float finalRot = deltaRot + currRot;
            //if (Mathf.Abs(deltaRot) > m_trackSpeed * Mathf.Deg2Rad * Time.fixedDeltaTime)
            //{
            //    if(deltaRot> 0.0f)
            //    {
            //        finalRot = currRot + m_trackSpeed * Mathf.Deg2Rad * Time.fixedDeltaTime;
            //    }
            //    else
            //    {
            //        finalRot = currRot - m_trackSpeed * Mathf.Deg2Rad * Time.fixedDeltaTime;
            //    }
            //}

            ////Debug.Log("DeltaRot = " + targetRot + " - " + currRot + " = " + deltaRot);

            //Vector3 targetPos = new Vector3(Mathf.Sin(finalRot), Mathf.Cos(finalRot) , 0);
            ////Debug.DrawLine(transform.position, targetPos * 2 + transform.position, Color.red);
            ////Debug.DrawLine(transform.position, (Vector3)diff * 2 + transform.position, Color.blue);
            //m_cannonPart.transform.LookAt(transform.position + Vector3.forward, targetPos);
            float rotZ = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            Quaternion targetRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, rotZ + m_adjustmentAngle));

            ////Calculating rotation speed (REWORK WITH QUATERNION.ROTATETOWARDS(WAY EASIER))
            ////Alternative solution transform.LookAt()

            m_cannonPart.transform.rotation = Quaternion.RotateTowards(m_cannonPart.transform.rotation, targetRotation, m_trackSpeed * Time.fixedDeltaTime);

            //Check if able to fire at the target
            if (m_target.isActiveAndEnabled  && m_cannonPart.transform.rotation.eulerAngles.z == targetRotation.eulerAngles.z && Vector2.Distance(transform.position, m_target.transform.position) <= m_range)
            {
                FiringProtocol();
            }
        }
    }

    private void FiringProtocol()
    {
        if(m_reloadingTime <= 0.0f)
        {
            //Fire the weapon, deal damage
            if (m_continuousFire)
            {
                m_target.TakeDamage(m_damagePerShot * Time.fixedDeltaTime, m_damageType);
            }
            else
            {
                m_target.TakeDamage(m_damagePerShot, m_damageType);
                m_reloadingTime = 60.0f / m_fireRate;
            }
            Debug.DrawLine(transform.position, m_target.transform.position, Color.red);
        }
        else
        {
            m_reloadingTime = m_reloadingTime - Time.fixedDeltaTime;
        }
    }

    public SpawnManager World
    {
        set
        {
            m_world = value;
        }
    }

}
