﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour
{

    public static PoolManager current;

    public string[] m_names;
    public GameObject[] m_pooledObjects;
    public int[] m_poolAmounts;

    private Hashtable m_mainPool = new Hashtable();
    private List<GameObject> m_tempList;

    void Awake()
    {
        current = this;

        m_tempList = new List<GameObject>();

        for (int i = 0; i < m_names.Length; i++)
        {
            IList<GameObject> objList = new List<GameObject>();

            for (int j = 0; j < m_poolAmounts[i]; j++)
            {
                GameObject obj = (GameObject)Instantiate(m_pooledObjects[i]);
                obj.SetActive(false);
                objList.Add(obj);
            }

            m_mainPool.Add(m_names[i], objList);
        }
    }

    public GameObject GetPooledObject(string name)
    {
        if (m_mainPool.ContainsKey(name))
        {
            m_tempList = m_mainPool[name] as List<GameObject>;

            for (int i = 0; i < m_tempList.Count; i++)
            {
                if (m_tempList[i] != null)
                {
                    if (!m_tempList[i].activeInHierarchy)
                    {
                        return m_tempList[i];
                    }
                }
            }
        }
        return null;
    }

    public void ResetPool()
    {
        for (int i = 0; i < m_names.Length; i++)
        {
            m_tempList = m_mainPool[name[i]] as List<GameObject>;

            for (int j = 0; j < m_tempList.Count; j++)
            {
                if (m_tempList[j].activeInHierarchy)
                {
                    m_tempList[j].SetActive(false);
                }
            }
        }
    }
}
