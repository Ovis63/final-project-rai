﻿using UnityEngine;
using System.Collections;
using TDEnums;

public class TileInfo : MonoBehaviour {
    [SerializeField]
    private TileType m_tileState;
    [SerializeField]
    private MoveDirection m_direction = MoveDirection.UP;

    private Turret m_turretPresent;

    public Turret TurretPresent
    {
        get
        {
            return m_turretPresent;
        }

        set
        {
            m_turretPresent = value;
        }
    }

    public TileType TileState
    {
        get
        {
            return m_tileState;
        }
    }

    public MoveDirection Direction
    {
        get
        {
            return m_direction;
        }
    }
}
