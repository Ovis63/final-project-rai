﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubwaveManager : MonoBehaviour {

    public Subwave[] m_lowCostSubwaves = new Subwave[2];

    void Start()
    {
        Subwave tempSubwave = new Subwave();
        tempSubwave.m_unitList.Add("Light Armored Unit");
        tempSubwave.m_subwaveCost = 50.0f;
        m_lowCostSubwaves[0] = tempSubwave;

        Subwave temp2Subwave = new Subwave();
        temp2Subwave.m_unitList.Add("Light Shielded Unit");
        temp2Subwave.m_subwaveCost = 60.0f;
        m_lowCostSubwaves[1] = temp2Subwave;

    }


}
