﻿namespace TDEnums
{
    public enum TileType
    {
        LAND,
        ROAD,
        WATER,
        SPAWN,
        GOAL
    };

    public enum DamageType
    {
        PHYSICAL,
        ENERGY,
        HYBRID
    };

    public enum MoveDirection
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    };
}
