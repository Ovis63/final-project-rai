﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TDEnums;

public class EnemyUnit : MonoBehaviour {

    [SerializeField]
    private SpawnManager m_world;
    [SerializeField]
    private GameObject m_shield;

    private Vector2 m_targetDestination;
    private float m_distanceTravelled = 0.0f;
    private MoveDirection m_currentDirection;   

    [Space(14)]
    [SerializeField]
    private float m_startingSpeed;              //Squares per second
    [SerializeField]
    private float m_purchaseCost;
    [SerializeField]
    private float m_startingPhysicalArmor;
    [SerializeField]
    private float m_startingEnergyArmor;

    private float m_physicalArmor;
    private float m_energyArmor;
    private float m_speed;

    //Methods
    void OnEnable()
    {
        m_physicalArmor = m_startingPhysicalArmor;
        m_energyArmor = m_startingEnergyArmor;
        m_distanceTravelled = 0.0f;
        m_speed = m_startingSpeed;

        if (m_world != null)
        {
            m_world.AddEnemyToActiveList(this);
        }
        m_targetDestination = transform.position;

        if(m_energyArmor > 0)
        {
            m_shield.SetActive(true);
        }
        else
        {
            m_shield.SetActive(false);
        }
    }

    void OnDisable()
    {
        if (m_world != null)
        {
            m_world.RemoveEnemyFromActiveList(this);
        }
    }

    void Update()
    {
        if (m_shield.activeInHierarchy && EnergyArmor == 0)
        {
            m_shield.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        try
        {
            //If we have no destination, we look at the floor as go where it says.
            if ((Vector2)transform.position == m_targetDestination)
            {
                TileInfo tempTile;
                if (Physics2D.OverlapPoint(transform.position) != null)
                {
                    tempTile = Physics2D.OverlapPoint(transform.position).GetComponent<TileInfo>();
                }
                else
                {
                    //Debug.Log("No tile found!");
                    return;
                }

                //Check if at the goal
                if (tempTile.TileState == TileType.GOAL)
                {
                    m_world.UnitReachedGoal++;
                    gameObject.SetActive(false);
                }

                Vector2 tempVector = new Vector2();
                switch (tempTile.Direction)
                {
                    case MoveDirection.UP:
                        tempVector = new Vector2(0.0f, 1.0f);
                        transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                        break;
                    case MoveDirection.DOWN:
                        tempVector = new Vector2(0.0f, -1.0f);
                        transform.rotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
                        break;
                    case MoveDirection.LEFT:
                        tempVector = new Vector2(-1.0f, 0.0f);
                        transform.rotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);
                        break;
                    case MoveDirection.RIGHT:
                        tempVector = new Vector2(1.0f, 0.0f);
                        transform.rotation = Quaternion.Euler(0.0f, 0.0f, 270.0f);
                        break;
                }



                m_targetDestination = (Vector2)tempTile.transform.position + tempVector;
            }

            if(PhysicalArmor <= 0.0f)
            {
                m_world.WavePass = DistanceTravelled;
                gameObject.SetActive(false);
            }

            //Actual movement
            //Calculate the lerp
            float tempDist = Vector2.Distance(transform.position, m_targetDestination);
            float moveLerp = 1.0f / ((tempDist / m_speed) / Time.fixedDeltaTime);

            Vector2 move = Vector2.Lerp(transform.position, m_targetDestination, moveLerp);
            m_distanceTravelled += Vector2.Distance((Vector2)transform.position, move);
            transform.position = move;
        }
        catch (MissingReferenceException ex) { }
    }

    public void TakeDamage(float damageValue, DamageType type)
    {
        if (EnergyArmor > 0.0f)
        {
            if (type == DamageType.PHYSICAL)
            {
                float tempHealth = EnergyArmor - (damageValue * 0.3f);
                if (tempHealth < 0.0f)
                {
                    EnergyArmor = 0.0f;
                    PhysicalArmor -= tempHealth;
                }
                else
                {
                    EnergyArmor = tempHealth;
                }
            }
            else if (type == DamageType.ENERGY)
            {
                float tempHealth = EnergyArmor - damageValue;
                if (tempHealth < 0.0f)
                {
                    EnergyArmor = 0.0f;
                    PhysicalArmor -= tempHealth * 0.3f;
                }
                else
                {
                    EnergyArmor = tempHealth;
                }
            }
        }
        else {
            if (type == DamageType.PHYSICAL)
            {
                PhysicalArmor -= damageValue;
            }
            else if (type == DamageType.ENERGY)
            {
                PhysicalArmor -= damageValue * 0.3f;
            }
        }
    }



    //Parameters
    public float PhysicalArmor
    {
        get
        {
            return m_physicalArmor;
        }
        set
        {
            m_physicalArmor = value;
        }
    }

    public float EnergyArmor
    {
        get
        {
            return m_energyArmor;
        }
        set
        {
            m_energyArmor = value;
        }
    }

    public float DistanceTravelled
    {
        get
        {
            return m_distanceTravelled;
        }
    }

    public SpawnManager World
    {
        set
        {
            m_world = value;
        }
    }
}
