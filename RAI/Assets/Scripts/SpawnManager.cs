﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private List<string> m_testWave;
    [SerializeField]
    private int m_ticksBetweenSpawns = 1;

    [SerializeField]
    private bool m_isSimulation = false;
    private WaveNode m_simulationWave;

    //All spawning time is done in ticks. There is one tick per fixedupdate. Fixedupdates are stable in timing.
    private int m_ticksLeft = 0;
    private int m_unitID = 0;
    private int m_numberOfWaves = 0;


    [SerializeField]
    private int m_waveManagerID;
    private int m_reachedGoalCount = 0;
    private float m_maxDistanceValue = 0.0f;
    private float m_waveScore = 0.0f;

    private List<EnemyUnit> m_currentEnemyList = new List<EnemyUnit>();

    //private bool notPrinted = false;
    private bool m_waveActive = false;
    private bool m_allEnemiesDead = false;

    void FixedUpdate()
    {
        if (m_waveActive == true)
        {
            SpawnUnits();
        }

        if(m_allEnemiesDead && m_unitID >= m_testWave.Count)
        {
            m_waveActive = false;
            m_waveScore = m_maxDistanceValue + (m_reachedGoalCount * 100);
            m_simulationWave.IndividualScore = m_waveScore;
        }
    }

    private void SpawnUnits()
    {
        if (m_ticksLeft <= 0)
        {
            GameObject temp = PoolManager.current.GetPooledObject(m_testWave[m_unitID]);
            temp.transform.position = transform.position;
            temp.GetComponent<EnemyUnit>().World = this;
            temp.SetActive(true);
            m_unitID++;
            m_ticksLeft = m_ticksBetweenSpawns;
        }
        else
        {
            m_ticksLeft--;
        }
    }

    public void RunSimulation(WaveNode unitList)
    {
        SimulationWave = unitList;
        m_waveActive = true;
    }

    public void AddEnemyToActiveList(EnemyUnit enemy)
    {
        m_currentEnemyList.Add(enemy);
        m_allEnemiesDead = false;
    }

    public void RemoveEnemyFromActiveList(EnemyUnit enemy)
    {
        m_currentEnemyList.Remove(enemy);

        if(m_currentEnemyList.Count == 0)
        {
            m_allEnemiesDead = true;
        }
    }

    //Parameters

    public List<EnemyUnit> CurrentEnemyList
    {
        get
        {
            return m_currentEnemyList;
        }
    }

    public WaveNode SimulationWave
    {
        set
        {
            m_simulationWave = value;
        }
    }

    public float WavePass
    {
        get
        {
            return m_maxDistanceValue;
        }
        set
        {
            if (value > m_maxDistanceValue)
            {
                m_maxDistanceValue = value;
                //Debug.Log("New Distance " + m_waveManagerID + ": " + m_maxDistanceValue);
            }
        }
    }

    public bool SimRunning
    {
        set
        {
            m_waveActive = value;
        }
    }

    public int UnitReachedGoal
    {
        set
        {
            m_reachedGoalCount = value;
        }

        get
        {
            return m_reachedGoalCount;
        }
    }


}
