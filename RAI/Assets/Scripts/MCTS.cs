﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCTS : MonoBehaviour {

    [SerializeField]
    public SubwaveManager m_subwaveManager;
    [SerializeField]
    private SpawnManager m_spawnManager;
    [SerializeField]
    private float m_totalCurrency;

    private float m_availableCurrency;
    private WaveNode m_bestWave;
    private WaveNode m_originNode;

    private bool testBool = false;    

    void Start()
    {
        m_totalCurrency = 150;
        m_originNode = new WaveNode(m_totalCurrency);
    }

    void Update()
    {
        if (!testBool)
        {
            m_originNode.PopulateChildrenNodes(m_subwaveManager.m_lowCostSubwaves);
            testBool = true;
        }
    }

    private void SimulateNode(WaveNode node)
    {
        m_spawnManager.SimulationWave = node;
        m_spawnManager.SimRunning = true;
    }


}
