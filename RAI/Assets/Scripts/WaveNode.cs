﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveNode
{
    private List<string> m_unitList;
    private float m_remainingCurrency;
    private List<WaveNode> m_childrenNodes;
    private float m_individualScore;
    private float m_cumulativeScore;
    private int m_simCounter;
    private bool m_finished;
    

    public WaveNode(List<string> units, float currency)
    {
        m_unitList = units;
        m_remainingCurrency = currency;
        m_childrenNodes = new List<WaveNode>();
        m_individualScore = 0;
        m_cumulativeScore = 0;
        m_simCounter = 0;
        m_finished = false;
    }

    public WaveNode(float currency)
    {
        m_remainingCurrency = currency;
        m_unitList = new List<string>();
        m_childrenNodes = new List<WaveNode>();
        m_remainingCurrency = currency;
        m_individualScore = 0;
        m_cumulativeScore = 0;
        m_simCounter = 0;
        m_finished = false;
    }

    public int PopulateChildrenNodes(Subwave[] subwaves)
    {
        int childNodeCount = 0;
        for(int i=0; i<subwaves.Length; i++)
        {
            if (m_remainingCurrency - subwaves[i].m_subwaveCost >= 0)
            {
                List<string> tempArray = new List<string>(m_unitList);
                foreach(string unit in subwaves[i].m_unitList)
                {
                    tempArray.Add(unit);
                }
                WaveNode tempNode = new WaveNode(tempArray, m_remainingCurrency - subwaves[i].m_subwaveCost);
                m_childrenNodes.Add(tempNode);
                childNodeCount++;
            }
        }

        if(childNodeCount == 0)
        {
            m_finished = true;
        }
        Debug.Log("Populated node with children nodes: " + childNodeCount);

        return childNodeCount;
    }


    //Parameters
    public List<string> UnitList
    {
        get
        {
            return m_unitList;
        }
    }

    public float IndividualScore
    {
        get
        {
            return m_individualScore;
        }
        set
        {
            m_individualScore = value;
        }
    }


}
